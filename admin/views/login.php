<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>

    <?php require 'requirements/header.html'; ?>
  </head>
  <body>
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-5 col-md-offset-3">
          <div class="panel panel-default login-panel">
            <div class="panel-heading">
              <p>Login op het Admin Panel!</p>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-10">
                  <form class="form" action="" method="POST">
                    <div class="form-group">
                      <label for="email" class="label-control">E-mail:</label>
                      <div class="input-group">
                        <input type="email" class="form-control" name="email" placeholder="E-mail adres" aria-describedby="basic-addon1">
                        <span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-user"></i></span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-10">
                    <div class="form-group">
                      <label for="password" class="label-control">Wachtwoord:</label>
                      <div class="input-group">
                        <input type="password" class="form-control" name="password" placeholder="Wachtwoord" aria-describedby="basic-addon1">
                        <span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-lock"></i></span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4">
                    <button type="submit" name="login" class="btn btn-primary">Inloggen</button>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-10">
                    <a href="#">Ik ben mijn wachtwoord vergeten.</a>

                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <?php require 'requirements/footer.html'; ?>
  </body>
</html>
