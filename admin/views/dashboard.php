<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>

    <?php require 'requirements/header.html'; ?>
  </head>
  <body>
    <div class="navbar navbar-default navbar-static-top top-navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="dashboard">Admin Panel</a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="nav" aria-expanded="false">
            <span class="sr-only">Toggle Navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>

        <div class="collapse navbar-collapse" id="nav">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Welcome, name</a></li>
          </ul>
        </div>
      </div>
    </div>

    <div class="section">
      <div class="row">
        <div class="col-md-3 left-section">
          <div class="navbar navbar-default navbar-static-top side-navigation">
            <ul class="nav sidebar-nav">
              <li class="active"><a href="dahsboard">Dashboard</a></li>
              <li><a href="gebruikers">Gebruikers</a></li>
            </ul>
          </div>
        </div>
        <div class="col-md-9 right-section">
          <div class="col-md-4">
            <div class="panel panel-default">
              <div class="panel-heading">
                <p>Homepagina</p>
              </div>
              <div class="panel-body">
                <p><a href="#">Homepagina aanpassen</a></p>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="panel panel-default">
              <div class="panel-heading">
                <p>Zenders-pagina</p>
              </div>
              <div class="panel-body">
                <p><a href="#">Zenders-pagina aanpassen</a></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php require 'requirements/footer.html'; ?>
  </body>
</html>
