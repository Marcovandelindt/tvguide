<?php

class DashboardController extends Controller {

  public function __construct() {
    parent::__construct();

    $this->load->Model('Dashboard');

    $dashboard = new Dashboard();

    $data['title']      = 'Dashboard!';
    $data['dashboard']  = $dashboard;

    $this->load->View('dashboard', $data);
  }
}
