<?php

class LoginController extends Controller {

	public function __construct() {
		parent::__construct();

		$this->load->Model('User');

    $user = new User();
		$user->Login();

		$data['title'] = 'Login!';
    $data['user'] = $user;

		$this->load->View('login', $data);
	}
}
