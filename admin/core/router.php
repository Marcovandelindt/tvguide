<?php

class Router {

    public function __construct() {
    $this->request = new Request();
    $this->LoadController($this->request->get('page'));
    }

    public function LoadController($class) {

      // Check if the Class is empty. If the class is empty, the Home Controller
      // wil be loaded

      if(empty($class)) {
        require BASE_PATH . '/controllers/login.php';
        $login = new LoginController();

        return false;
      }

      // Check if the Class exists. If the Class exists, the right Controller
      // will be loaded

      if(file_exists(BASE_PATH . '/controllers/' . $class . '.php')) {
        require BASE_PATH . '/controllers/' . $class . '.php';

        $controller = $class . 'Controller';
        $class      = new $controller;

        return false;
      }

      // If the requested Class does not exists, the Error Controller will be
      // loaded

      if(!file_exists(BASE_PATH . '/controllers/' . $class . '.php') &&
      !empty($class)) {
        require BASE_PATH . '/errors/404.php';
      }
    }
}

# End of the File
