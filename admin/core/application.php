<?php

class Application {

  public function __construct() {
    $this->LoadPaths();
    $this->load   = new Loader();
    $this->router = new Router();
  }

  public function LoadPaths() {
    require '../database/general.php';
    require '../database/connection.php';
    require BASE_PATH . '/core/controller.php';
    require BASE_PATH . '/core/model.php';
    require BASE_PATH . '/core/loader.php';
    require BASE_PATH . '/core/router.php';
    require BASE_PATH . '/core/request.php';
  }
}
