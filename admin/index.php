<?php

session_start();

ini_set('display_errors', 1);
error_reporting(E_ALL);

define('BASE_PATH', __DIR__);

require BASE_PATH . '/core/application.php';

$application = new Application();
