<?php

class HomeController extends Controller {

	public function __construct() {
		parent::__construct();

		$this->load->Model('Home');

		$home = new Home();

		$data['title'] = 'Home';
		$data['home'] = $home;

		$this->load->View('home', $data);
	}
}
