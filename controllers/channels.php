<?php

class ChannelsController extends Controller {

  public function __construct() {
    parent::__construct();

    $this->load->Model('Channels');
    $channels = new Channels();

    $data['title'] = 'Zenders';
    $data['channels'] = $channels;

    echo $this->channel();
    
    $this->load->View('channels', $data);
  }

  private function channel() {
    if($this->request->get('value')) {
      return $this->request->get('value');
    }
  }
}
