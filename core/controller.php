<?php

class Controller {
  public function __construct() {
    $this->load     = new Loader();
    $this->request  = new Request();
  }
}

# End of the File
