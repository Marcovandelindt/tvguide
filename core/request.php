<?php

class Request {

  public function get($key) {
    if(array_key_exists($key, $_REQUEST)) {
      return $_REQUEST[$key];
    }
  }

  public function IsPost() {
    return $_SERVER['REQUEST_METHOD'] == 'POST';
  }

  public function IsGet() {
    return $_SERVER['REQUEST_METHOD'] == 'GET';
  }
}

# End of the File
