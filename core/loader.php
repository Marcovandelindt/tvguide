<?php

class Loader {

  public function View($name, $param = NULL) {
    if(isset($param)) {
      extract($param);
    }

    // Check if the Requested exists

    if(file_exists(BASE_PATH . '/views/' . $name . '.php')) {
      require BASE_PATH . '/views/' . $name . '.php';
    }
  }

  public function Model($name, $param = NULL) {
    if(isset($param)) {
      extract($param);
    }

    // Check if the Requested Model exists

    if(file_exists(BASE_PATH . '/models/' . $name . '.php')) {
      require BASE_PATH . '/models/' . $name . '.php';

      $name = new $name;
    }
  }
}

# End of the File
