<?php

class Home extends Model {

  public function GetActiveCarouselItem() {
    $get = $this->connect->database->prepare('SELECT * FROM carousel WHERE
                                              status = "active"');
    $get->execute();

    foreach($get as $item) {
      echo '
        <div class="item active">
          <a href="' . $item['link'] . '">
            <img src="assets/img/programmas/' . $item['image'] . '.jpg"
            class="carousel-image d-block img-fluid">
            <div class="carousel-caption">
              <h3>' . $item['title'] . '</h3>
              <p>' . $item['channel'] . ', ' . $item['time'] . '
            </div>
          </a>
        </div>
      ';
    }
  }

  public function GetCarouselItems() {
    $get = $this->connect->database->prepare('SELECT * FROM carousel WHERE
                                              status = ""');
    $get->execute();

    foreach($get as $item) {
      echo '
        <div class="item">
          <a href="' . $item['link'] . '">
            <img src="assets/img/programmas/' . $item['image'] . '.jpg"
            class="carousel-image d-block image-fluid">
            <div class="carousel-caption">
              <h3>' . $item['title'] . '</h3>
              <p>' . $item['channel'] . ', ' . $item['time'] . '
            </div>
          </a>
        </div>
      ';
    }
  }

  public function GetPopularTodayItems() {
    $get = $this->connect->database->prepare('SELECT * FROM popular_today');
    $get->execute();

    foreach($get as $item) {
      echo '
        <div class="media">
          <div class="media-left">
            <img src="assets/img/programmas/' . $item['image'] . '"
            class="media-object">
          </div>
          <div class="media-body">
            <h4 class="media-heading">' . $item['title'] . '</h4>
            <p class="media-content">' . $item['channel'] . ', ' . $item['time']
            . '</p>
          </div>
        </div>
      ';
    }
  }

  public function GetTopMovieToday() {
    $get = $this->connect->database->prepare('SELECT * FROM movies WHERE type
                                              = "top"');
    $get->execute();

    foreach($get as $item) {
      echo '
        <img src="assets/img/programmas/' . $item['image'] . '.jpg"
        class="top-movie-image">
        <div class="caption">
          <h3>' . $item['title'] . '</h3>
          <p class="channel-time">' . $item['channel'] . ', ' . $item['time'] . '
          &nbsp;<span class="label label-primary">' . $item['genre'] . '</span></p>
          <p class="description">' . $item['description'] . '</p>
        </div>
      ';
    }
  }

  public function GetTopMoviesToday() {
    $get = $this->connect->database->prepare('SELECT * FROM movies WHERE type =
      ""');
    $get->execute();

      foreach($get as $item) {
        echo '
          <div class="media">
            <div class="media-left">
              <img class="media-object" src="assets/img/programmas/' .
              $item['image'] . '.jpg">
            </div>
            <div class="media-body">
              <h4 class="media-heading">' . $item['title'] . '</h4>
              <p>' . $item['channel'] . ', ' . $item['time'] . '</p>
              <span class="label label-primary">' . $item['genre'] . '</span>
            </div>
          </div>
        ';
      }
  }

  public function GetTopSportToday() {
    $get = $this->connect->database->prepare('SELECT * FROM sports WHERE state
    = "top"');
    $get->execute();

    foreach($get as $item) {
      echo '
        <img src="assets/img/programmas/' . $item['image'] . '.jpg"
        class="top-sport-image">
        <div class="caption">
          <h3>' . $item['title'] . '</h3>
          <p class="channel-time">' . $item['channel'] . ', ' . $item['time'] . '
          &nbsp;<span class="label label-primary">' . $item['type'] . '</span></p>
          <p class="description">' . $item['description'] . '</p>
        </div>
      ';
    }
  }

  public function GetTopSportsToday() {
    $get = $this->connect->database->prepare('SELECT * FROM sports WHERE state
    = ""');
    $get->execute();

    foreach($get as $item) {
        echo '
          <div class="media">
            <div class="media-left">
              <img class="media-object" src="assets/img/programmas/' .
              $item['image'] . '.jpg">
            </div>
            <div class="media-body">
              <h4 class="media-heading">' . $item['title'] . '</h4>
              <p>' . $item['channel'] . ', ' . $item['time'] . '</p>
              <span class="label label-primary">' . $item['type'] . '</span>
            </div>
          </div>
        ';
    }
  }

  public function GetRecentNews() {
    $get = $this->connect->database->prepare('SELECT * FROM news ORDER BY date DESC;');
    $get->execute();

    foreach($get as $item) {
      echo '
        <div class="media">
          <div class="media-left">
            <img class="media-object" src="assets/img/nieuws/' .
            $item['image'] . '.jpg">
          </div>
          <div class="media-body">
            <h4 class="media-heading">' . $item['title'] . '</h4>
            <p>' . $item['description'] . '</p>
          </div>
        </div>
      ';
    }
  }
}
