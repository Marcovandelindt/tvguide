<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>

    <?php require 'requirements/header.html'; ?>
  </head>
  <body>
    <div class="container main-wrapper">
      <div class="inner-wrapper">
        <div class="header">
            <p class="logo">TV<span class="logo-color">Gids</span></p>
            <p class="additional">Here comes the additional information!</p>
        </div>
        <nav class="navbar navbar-inverse navbar-static-top">
          <div class="container-fluid">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav" aria-expanded="false">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </div>
            <div class="collapse navbar-collapse" id="nav">
              <ul class="nav navbar-nav navbar-left">
                <li><a href="home">Home</a></li>
                <li class="active"><a href="#">Zenders</a></li>
                <li><a href="#">Test</a></li>
                <li><a href="#">Test</a></li>
                <li><a href="#">Test</a></li>
                <li><a href="#">Test</a></li>
              </ul>
              <form class="navbar-form navbar-right">
                <div class="input-group">
                  <input type="text" class="form-control" name="search" placeholder="Zoeken..." aria-describedby="basic-addon1">
                  <span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-search"></i></span>
                </div>
              </form>
            </div>
          </div>
        </nav>

        <div class="section">
          <div class="col-md-12">
            <div class="jumbotron">
              <h1>Zenderoverzicht</h1>
              <p>Op deze pagina vind u een overzicht van de zenders die momenteel op TVGids ondersteund worden. De gebruikelijke zenders
                zoals NPO 1, NPO 2 en NPO 3 zijn aanwezig, net zoals de RTL en SBS kanalen. Daarnaast vindt u ook nog kanalen als Comedy
                Central, Nickelodeon, MTV en noem maar op. Scroll heen en weer en zoek zo de zender waarnaar u op zoek bent.</p>
            </div>
          </div>
          <div class="col-md-8 left-section">
            <div class="row">
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-3">
                    <div class="thumbnail">
                      <a href="<?php echo BASE_PATH; ?>/channel/npo1">
                        <img src="assets/img/logos/npo1.png" class="channel-logo" alt="NPO 1">
                        <div class="caption">
                          NPO 1
                        </div>
                      </a>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="thumbnail">
                      <a href="#">
                        <img src="assets/img/logos/npo2.png" class="channel-logo" alt="NPO 2">
                        <div class="caption">
                          NPO 2
                        </div>
                      </a>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="thumbnail">
                      <a href="#">
                        <img src="assets/img/logos/npo3.png" class="channel-logo" alt="NPO 3">
                        <div class="caption">
                          NPO 3
                        </div>
                      </a>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="thumbnail">
                      <a href="#">
                        <img src="assets/img/logos/rtl4.png" class="channel-logo" alt="RTL 4">
                        <div class="caption">
                          RTL 4
                        </div>
                      </a>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-3">
                    <div class="thumbnail">
                      <a href="#">
                        <img src="assets/img/logos/rtl5.png" class="channel-logo" alt="RTL 5">
                        <div class="caption">
                          RTL 5
                        </div>
                      </a>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="thumbnail">
                      <a href="#">
                        <img src="assets/img/logos/sbs6.png" class="channel-logo" alt="SBS 6">
                        <div class="caption">
                          SBS 6
                        </div>
                      </a>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="thumbnail">
                      <a href="#">
                        <img src="assets/img/logos/rtl7.png" class="channel-logo" alt="RTL 7">
                        <div class="caption">
                          RTL 7
                        </div>
                      </a>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="thumbnail">
                      <a href="#">
                        <img src="assets/img/logos/rtl8.png" class="channel-logo" alt="RTL 8">
                        <div class="caption">
                          RTL 8
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    <div class="thumbnail">
                      <a href="#">
                        <img src="assets/img/logos/net5.png" class="channel-logo" alt="NET 5">
                        <div class="caption">
                          NET 5
                        </div>
                      </a>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="thumbnail">
                      <a href="#">
                        <img src="assets/img/logos/veronica.png" class="channel-logo" alt="Veronica">
                        <div class="caption">
                          Veronica
                        </div>
                      </a>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="thumbnail">
                      <a href="#">
                        <img src="assets/img/logos/sbs9.png" class="channel-logo" alt="SBS 9">
                        <div class="caption">
                          SBS 9
                        </div>
                      </a>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="thumbnail">
                      <a href="#">
                        <img src="assets/img/logos/fox.png" class="channel-logo" alt="FOX">
                        <div class="caption">
                          FOX
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-3 right-section">
            <div class="col-md-12">
              <div class="panel panel-default poll-panel" style="margin-top: 0px !important">
                <div class="panel-heading">
                  <p>Vanavond kijk ik naar:</p>
                </div>
                <div class="panel-body">
                  <div class="form">
                    <div class="form-group">
                      <input type="radio" name="vote" value="0" onclick="getVote(this.value)">
                      Ik kijk een film
                    </div>
                    <div class="form-group">
                      <input type="radio" name="vote" value="1" onclick="getVote(this.value)">
                      Ik kijk sport
                    </div>
                    <div class="form-group">
                      <input type="radio" name="vote" value="2" onclick="getVote(this.value)">
                      Ik kijk een serie
                    </div>
                    <div class="form-group">
                      <input type="radio" name="vote" value="3" onclick="getVote(this.value)">
                      Ik kijk een documentaire
                    </div>
                    <div class="form-group">
                      <input type="radio" name="vote" value="4" onclick="getVote(this.value)">
                      Ik kijk geen televisie
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
