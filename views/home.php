<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>

    <?php require 'requirements/header.html'; ?>
  </head>
  <body>
    <div class="container main-wrapper">
      <div class="inner-wrapper">
        <div class="header text-center">
            <p class="logo">TV<span class="logo-color">Gids</span></p>
            <p class="additional">Here comes the additional information!</p>
        </div>
        <nav class="navbar navbar-inverse navbar-static-top">
          <div class="container-fluid">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav" aria-expanded="false">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </div>
            <div class="collapse navbar-collapse" id="nav">
              <ul class="nav navbar-nav navbar-left">
                <li class="active"><a href="home">Home</a></li>
                <li><a href="channels">Zenders</a></li>
                <li><a href="#">Test</a></li>
                <li><a href="#">Test</a></li>
                <li><a href="#">Test</a></li>
                <li><a href="#">Test</a></li>
              </ul>
              <form class="navbar-form navbar-right">
                <div class="input-group">
                  <input type="text" class="form-control" name="search" placeholder="Zoeken..." aria-describedby="basic-addon1">
                  <span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-search"></i></span>
                </div>
              </form>
            </div>
          </div>
        </nav>

        <div class="section">
          <div id="row">
            <div class="col-md-8 left-section">
              <div id="carousel" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                  <li data-target="#carousel" data-slide-to="0" class="active"></li>
                  <li data-target="#carousel" data-slide-to="1"></li>
                  <li data-target="#carousel" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                  <?php echo $home->GetActiveCarouselItem(); ?>
                  <?php echo $home->GetCarouselItems(); ?>
                </div>
              </div>

              <div class="row" style="margin-top: 50px;">
                <div class="col-md-6">
                  <div class="thumbnail top-movies">
                    <?php echo $home->GetTopMovieToday(); ?>
                    <hr>
                    <div class="media-list">
                      <?php echo $home->GetTopMoviesToday(); ?>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="thumbnail top-sports">
                    <?php echo $home->GetTopSportToday(); ?>
                    <hr>
                    <div class="media-list">
                      <?php echo $home->GetTopSportsToday(); ?>
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="media-list recent-news" style="margin-top: 50px;">
                    <div class="recent-news-header">
                      <p>Laatste Nieuws!</p>
                    </div>
                    <?php echo $home->GetRecentNews(); ?>
                    <div class="see-more">
                      <p><a href="#">Bekijk het gehele nieuwsoverzicht!</a></p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4 right-section">
              <div class="col-md-12">
                <div class="media-list popular-today">
                  <div class="popular-today-heading">
                    <p>Populair Vandaag</p>
                  </div>
                  <?php echo $home->GetPopularTodayItems(); ?>
                  <div class="see-more">
                    <p>Bekijk de complete TV Gids</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="ad">
                  <img src="assets/img/ads/ad.gif">
                </div>
              </div>
              <div class="col-md-12">
                <div class="panel panel-default poll-panel">
                  <div class="panel-heading">
                    <p>Vanavond kijk ik naar:</p>
                  </div>
                  <div class="panel-body">
                    <div class="form">
                      <div class="form-group">
                        <input type="radio" name="vote" value="0" onclick="getVote(this.value)">
                        Ik kijk een film
                      </div>
                      <div class="form-group">
                        <input type="radio" name="vote" value="1" onclick="getVote(this.value)">
                        Ik kijk sport
                      </div>
                      <div class="form-group">
                        <input type="radio" name="vote" value="2" onclick="getVote(this.value)">
                        Ik kijk een serie
                      </div>
                      <div class="form-group">
                        <input type="radio" name="vote" value="3" onclick="getVote(this.value)">
                        Ik kijk een documentaire
                      </div>
                      <div class="form-group">
                        <input type="radio" name="vote" value="4" onclick="getVote(this.value)">
                        Ik kijk geen televisie
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <ul class="list-group social-media-list">
                  <li class="list-group-item facebook"><i class="fa fa-facebook pull-left"></i><span class="text pull-right">Volg ons op Facebook!</span></li>
                  <li class="list-group-item twitter"><i class="fa fa-twitter pull-left"></i><span class="text pull-right">Volg ons op Twitter!</span></li>
                  <li class="list-group-item instagram"><i class="fa fa-instagram pull-left"></i><span class="text pull-right">Volg ons op Instagram!</span></li>
                  <li class="list-group-item youtube"><i class="fa fa-youtube pull-left"></i><span class="text pull-right">Volg ons op Youtube!</span></li>
                </ul>
              </div>
              <div class="col-md-12">
                <div clas="footer">

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>



    <?php require 'requirements/footer.html'; ?>
  </body>
</html>
