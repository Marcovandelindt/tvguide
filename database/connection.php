<?php

class Database {

  public $database;

  public function __construct() {
    try {
      $this->database = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME,
      DB_USERNAME, DB_PASSWORD);
      echo '<div class="connection-status">Connected to the Database!</div>';
    } catch(PDOException $error) {
        echo 'Could not connect to the Database: ' . $error->getMessage();
    }

    return $this->database;
  }

}
